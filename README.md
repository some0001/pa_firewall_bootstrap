The bootstrapped_deploy.bash script deploys Palo Alto Firewall virtual machine with the bootstrap configuration found in the bootstrap directory to the remote ESXI. Palo Alto Firewall ova must be present in the root directory
The bootstrap_attachher.bash script attaches bootstrap configuration from local directory to existing Palo Alto Firewall on remote ESXI

Bootstrap configuration can be changed within bootstrap directory.  

Provided minimal configuration sets accessible management interface with the ip address of **192.168.1.100, gateway 192.168.1.1** and administrator user with login **admin** and password **Admin123**    
Bootstrap configuration guide: https://docs.paloaltonetworks.com/vm-series/9-0/vm-series-deployment/bootstrap-the-vm-series-firewall.html


**USAGE:**  
bash *.bash \<VM name> \<ESXI User> \<ESXI IP> \<ESXI PASS>

**REQUIREMENTS:**
*  OVFTOOL
*  sshpass
*  genisoimage
*  scp